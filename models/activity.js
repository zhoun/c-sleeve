import {Http} from "../utils/http";

class Activity {
    static LocationD = 'a-2'

    static async getHomeLocationD() {
        return await Http.request({
            url: `activity/name/${Activity.LocationD}`
        })
    }
}

export {
    Activity
}